#include <stdio.h>
#include "dice.h"

int main(){
    //This is a dice simulator
    initializeSeed();
    //Now you can have the number of faces
    printf("Number of faces: ");
    int faces;
    scanf("%d", &faces);
    printf("\nLet's roll the dice: %d\n", rollDice(faces));
    return 0;
}
